<div class="col-md-4">
    <div class="sidebar" id="sidebar">

        <section>
            <h2 class="title">BLOG</h2>
            <div class="mini-posts">


            <?php

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/1900889043568025/posts/?fields=full_picture%2Clink%2Cmessage%2Ccreated_time&access_token=281005105715384%7C1EqJBQns4oXYD8xZ5P5FUSKiank");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $data = curl_exec($ch);
                
                $array_fb = json_decode($data);

                $obj2 = $array_fb->{'data'};
                
                for ($i2 = 0; $i2 < count($obj2) && $i2 <= 6; $i2++) 
                {
                    $obj3 = $obj2[$i2];
                    $imagem = $obj3->{'full_picture'};
                    $texto = $obj3->{'message'};
                    $link = $obj3->{'link'};
                    $titulo = explode("\n", $texto); 
                    $data = $obj3->{'created_time'};

                    $hora = strtotime(substr($data,0,10));
                    $data = date('d-m-Y',$hora);
                    
                ?>

                <article class="mini-post">
                    <header>
                        <h3><a href="<?php echo $link[0] ?>"><?php echo $titulo[0] ?></a></h3>
                        <time class="published" datetime="<?php echo $data; ?>"><?php echo $data ?></time>
                    </header>
                    <a href="<?php echo $link ?>" class="image"><img src="<?php echo $imagem ?>" alt="<?php echo $titulo[0] ?>" /></a>
                </article>
        
            <?php  
                } 
                                    
                curl_close($ch);
            ?>

            </div>
        </section>

        <section>
            <div class="widget HTML">
                <h2 class="title">INSTAGRAM</h2>
                <div class="widget-content">
                    <div class="instagram-feeds row-gallery">
                        
                    <?php

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token=183068640.4ba0563.e83e240f956645d1b61dcd38c1d4722e",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache",
                            "postman-token: 5276c78a-88bf-f7e0-be4d-d7131380d2de"
                        ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        if ($err) 
                        {
                            echo "cURL Error #:" . $err;
                        } 
                        else 
                        {
                            $obj = json_decode($response);
                            $obj2 = $obj->{'data'};
                            
                            for ($i = 0; $i < 6; $i++) 
                            {

                                $obj3 = $obj2[$i];
                                $obj4 = $obj3->{'images'};

                                $obj5 = $obj4->{'standard_resolution'}; 
                                $url = $obj5->{'url'};

                    ?>
                        <a href="#" class="col-sm-4 col-xs-3"><div style="background:url('<?php echo $url ?>'); width: 100px; height: 100px; background-size: cover;"></div></a>

                    <?php 
                            }
                        }

                    ?>
                    </div>
                </div>
            </div>
        </section> <!-- End inta -->

    </div> <!-- End Sidebar -->
</div><!-- End-col-md-4 -->