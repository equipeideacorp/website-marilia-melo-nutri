<?php get_header();


/*Template Name: Página Inicial */

?>       
       
    <section id="sliderprincipal" class="rev_slider_wrapper slider-v2">
        <div id="slider-v2" class="rev_slider"  data-version="5.0">
            <ul>
                <li data-transition="random" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" >
                
                    <video loop autoplay style="width: 100%; filter: blur(10px); ">
                        <source src="<?php echo get_template_directory_uri() ?>/videos/slider.mp4" type="video/mp4">
                    </video>

                    <div id="box-slide" class="flexslider">
                        <ul class="slides">
                            <li>
                                <h3 style=" text-align:right; ">Bem Vindo   </h3>
                                <h1 style="font-size:50px !important">Nosso Site</h1>
                                <p> Aqui tratamos de alimentação saudável: dicas, receitas, vídeos, informações e muito mais!</p>
                                    </br>
                                <div class="garden-button" style="float:right">
                                    <a href="#sobrenos" class="smoothScroll" style="background: #e6a953; color: white;">Sobre a Nutri</a>
                                </div>
                            </li>
                            <li>
                                <h3>Passo a Passo </h3>
                                <h1 >Receitas</h1>
                                <p> Sua Nutri atacando de chef para preparar e ensinar receitas deliciosas e saudáveis.</p>
                                    </br>
                                <div class="garden-button" style="float:right">
                                    <a href="#tastenutri" class="smoothScroll" style="background: #c2de6f; color: white;">Receitas</a>
                                </div>
                            </li>
                            <li>
                                <h3>Curta </h3>
                                <h1 >TasteNutri!</h1>
                                <p> As melhores receitas em vídeo do <span style="font-style:italic">TasteMade</span>® selecionadas pela Nutri para você!</p>
                                  </br>
                                <div class="garden-button" style="float:right">
                                  <a href="#videos" class="smoothScroll" style="background: #c2de6f; color: white;">Vídeos</a>
                                </div>
                            </li>   
                            <li>
                                <h3>Agende</h3>
                                <h1 >Consulta!</h1>
                                <p> Marque online ou verifique disponibilidade de datas sem compromisso! </br> </p>
                                    </br>
                                <div class="garden-button" style="float:right">
                                    <a href="#idcontato" class="smoothScroll" style="background: #e6a953; color: white;">Agendar Consulta</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </section>

   <section id="image-mobile" style="height: 600px; ">
            <div style="100%">
                <img style="height: 100%" src="<?php echo get_template_directory_uri() ?>/images/fotosaladamobile.PNG">
            </div>

            <div id="box-slide" class="flexslider-mobile">
                <ul class="slides">
                    <li>
                        <h3>Bem Vindo   </h3>
                        <h1 style="position: relative;left: -15px;">Nosso Site</h1>
                        <p> Aqui tratamos de alimentação saudável: dicas, receitas, vídeos, informações e muito mais!</p>
                            </br>
                        <div class="garden-button" style="float:right">
                            <a href="#sobrenos" class="smoothScroll" style="background: #e6a953; color: white;">Sobre a Nutri</a>
                        </div>
                    </li>
                    <li>
                        <h3>Passo a Passo </h3>
                        <h1 style="position: relative;left: -15px;">Receitas</h1>
                        <p> Sua Nutri atacando de chef para preparar e ensinar receitas deliciosas e saudáveis.</p>
                        </br>
                        <div class="garden-button" style="float:right">
                            <a href="#tastenutri" class="smoothScroll" style="background: #c2de6f; color: white;">Receitas</a>
                        </div>
                    </li>
                    <li>
                        <h3>Curta</h3>
                        <h1 style="position: relative;left: -15px;">TasteNutri!</h1>
                        <p> As melhores receitas em vídeo do <span style="font-style:italic">TasteMade</span>® selecionadas pela Nutri para você!</p>
                        </br>
                        <div class="garden-button" style="float:right">
                            <a href="#videos" class="smoothScroll" style="background: #c2de6f; color: white;">Vídeos</a>
                        </div>
                    </li>
                    <li>
                        <h3>Agende</h3>
                        <h1 >Consulta!</h1>
                        <p> Marque online ou verifique disponibilidade de datas sem compromisso! </br> </p>
                            </br>
                        <div class="garden-button" style="float:right">
                            <a href="#idcontato" class="smoothScroll" style="background: #e6a953; color: white;">Agendar Consulta</a>
                        </div>
                    </li>
                </ul>
            </div>
    </section>

        <section style="height: 971px; background: url(<?php echo get_template_directory_uri() ?>/images/index-2/bg-sobre1.jpeg); background-size: cover; background-attachment: fixed; background-position: center; background-repeat: no-repeat;" id="sobrenos" class="about-v2">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-6 col-sm-12">
                        <div class="col-md-offset-3 col-md-7 col-sm-12">
                            <img style="box-shadow: 0px 0px 5px 2px white;" src="<?php echo get_template_directory_uri() ?>/images/fotomariliamelo.png ">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12" style="padding-right: 10%; padding-top:41px ">
                        <div class="box-content">
                            <h2 style="font-size:40px; padding-bottom: 20px; color:#FFF">Marília Melo</h2>
                            <p style="color:#FFF; font-size: 16px !important;" >Marília Melo é nutricionista formada pela Universidade de Fortaleza (UNIFOR), pós-graduada nas 
                                áreas de nutrição funcional e em nutrição esportiva. Atua em consultório com foco em emagrecimento, nutrição estética, 
                                melhora da performance esportiva, nutrição funcional, reeducação alimentar, prevenção e tratamento de doenças crônicas 
                                não transmissíveis (DCNT). </p><p style="color:#FFF; font-size: 16px !important;"> Acredita no poder dos alimentos e que uma alimentação saudável deve ser baseada principalmente 
                                no equilíbrio e no desenvolvimento de um novo estilo de vida, respeitando a individualidade de cada um com uma nutrição 
                                personalizada.</p>

                        </div>
                    </div>

                </div>
            </div>
        </section>

      
        
        <section id="servicos" class="why-choose-us">
            <div class="container">
                <div class="garden-title text-center">
                    <h3>Serviços da Nutri</h3>
                    <p>Confira as etapas da consulta nutricional:</p>
                </div>
                <ul class="row">
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                                <img style="filter: invert(1); width: 50px;" src="<?php echo get_template_directory_uri() ?>/images/checklist.png" alt="">
                            </div>
                            <div class="box-text">
                                <h3>Avaliação </br> Nutricional</h3>
                                <p>A Avaliação Nutricinoal envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Avaliação clínica;</li>
                                    <li>- Avaliação bioquímica;</li>
                                    <li>- Avaliação do consumo alimentar.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                               
                               <img style="filter: invert(1); width: 35px;" src="<?php echo get_template_directory_uri() ?>/images/balanca.png" alt="">
                            </div>
                            <div class="box-text">
                                <h3>Avaliação </br>  Física</h3>
                                <p>A Avaliação Física envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Avaliação antropométrica;</li>
                                    <li>- Avaliação da composição corporal;</li>
                                    <li>- Diagnóstico nutricional.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                                <span class="flaticon flaticon-fruit"></span>
                            </div>
                            <div class="box-text">
                                <h3>Prescrição de Plano Alimentar</h3>
                                <p>A Prescrição de Plano Alimentar envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Plano alimentar individualizado;</li>
                                    <li>-  Orientação nutricional;</li>
                                    <li>- Sugestões de receitas.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            

        </section>
        <section id="missao" class="our-mission" style="padding-bottom: 24px;">
            <div class="container">
                <div class="box">
                    <div class="box-mission text-center">
                        <div class="garden-title">
                            <h3 style=" padding-top: 28px;">A mudança do corpo começa no comportamento!</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       

        <section style="margin-top:5%" id="tastenutri" class="recent-projects-v2">
            <div class="container">
                <div class="garden-title text-center" style="padding-bottom: 30px">
                    <h3>Receitas da Nutri</h3>
                    <p style="text-align: center; padding: 10px 15% 0 15%;">Aqui você encontra receitas preparadas a mão pela sua nutri, a ideia é deliciosa, é mostrar que com os ingredientes certos e com o nosso passo a passo, qualquer um pode ser um autêntico "Chef Nutri" !</p>
                </div>
                
                <?php $tags = array('doces'=>'Doces','lowcarb'=>'Low Carb','saladas'=>'Saladas','salgadas'=>'Salgadas')  ?>

                <div class="garden-slick-menu" style="margin-bottom: 30px;"> 
                    <ul id="our_projects">
                        <li id="todos" class="active button-filter"><a href="">Todas</a></li>

                        <?php 

                            foreach ($tags as $key => $value)
                            {
                                echo '<li id="'.$key.'" class="button-filter"><a href="">'.$value.'</a></li>';
                            }

                        ?>
                    
                    </ul>
                </div>

                <div class="slick-content">
                    <ul class="slick-slider slick-our-projects-v2">
                        
                    <?php

                         // Facebook 


                        $facebook = curl_init();
                        curl_setopt($facebook, CURLOPT_URL, "https://graph.facebook.com/v2.10/1861689894158788/posts/?fields=full_picture%2Cmessage%2Clink&access_token=281005105715384%7C1EqJBQns4oXYD8xZ5P5FUSKiank");
                        curl_setopt($facebook, CURLOPT_RETURNTRANSFER, true);

                        $datafacebook = curl_exec($facebook);
                        $array_fb = json_decode($datafacebook);
                        $objface = $array_fb->{'data'};

                         
                                 
                        $doces   = "#doces";
                        $lowcarb   = "#lowcarb";
                        $saladas   = "#saladas";
                        $salgadas   = "#salgadas";
                        

                        for ($i=0; $i < count($objface); $i++) { 
                            if(strpos($objface[$i]->message, "#doces")){
                                $filtros .= 'filter-'. substr($doces, 1) . ' ';
                            }
                            if(strpos($objface[$i]->message, "#lowcarb")){
                                $filtros .= 'filter-'. substr($lowcarb, 1) . ' ';
                            }
                            if(strpos($objface[$i]->message, "#saladas")){
                                $filtros .= 'filter-'. substr($saladas, 1) . ' ';
                            }

                            if(strpos($objface[$i]->message, "#salgadas")){
                                $filtros .= 'filter-'. substr($salgadas, 1) . ' ';
                            }
                            
                       
                            if($filtros){
                                
                                $texto = nl2br($objface[$i]->message);
                                $imagem = $objface[$i]->full_picture;
                                $link = $objface[$i]->link;
                                $titulo = explode("\n", $objface[$i]->message); 
                                
                            ?>
                          
                                <li class="filter-todos <?php echo $filtros ?>">
                                    <div class="box" style="background-color: white; border-radius: 2px; height:346px; box-shadow: 0px 0px 20px -5px rgba(0,0,0,0.7);">
                                        <a href="#" style="color:inherit" data-toggle="modal" data-target="#myModal" titulo="<?php echo substr($titulo[0], 0, 100); ?>" conteudo="<?php echo $texto ?>" imagem="<?php echo $imagem ?>" class="modal-receita">
                                            <div class="efeito">
                                                <div class="garden-img-full-width efeito2" style="width: 280px;height: 240px; border-radius: 2px 2px 0px 0px; background: url(<?php echo $imagem ?>); background-size: cover;   background-position: right;"></div>
                                            </div>
                                        </a>
                                        <h3 style="text-align: center; font-weight: bold; padding: 5px;"><a href="#" data-toggle="modal" data-target="#myModal" titulo="<?php echo substr($titulo[0], 0, 100) . '...'; ?>" conteudo="<?php echo $texto ?>" imagem="<?php echo $imagem ?>" class="modal-receita link-receita"></br><?php echo substr($titulo[0], 0, 50) . '...'; ?></a></h3>
                                    </div>
                                </li>

                        <?php
                            $filtros = ""; 
                            }

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </section>

        <?php 
            $json = file_get_contents(get_site_url()."/tastenutri/json-videos/");
            $json_data = json_decode($json, true);
        ?>
        <section id="videos" class="our-videos-v2">
            <div class="container">
                <div class="garden-title text-center">
                    <img src="<?php echo get_template_directory_uri() ?>/images/tastenutri.png">
                    <p style="padding: 0px 15% 0 15%;">Amamos os vídeos do <span style="font-style:italic">TasteMade</span>®, então selecionamos os melhores para você, mas quando for preciso, faremos pequenas substituições ou adaptações nas receitas, pois não basta ter <span style="font-style:italic">Taste</span>, tem que ser Nutri!</p>
                    <p style="padding: 0px 15% 0 15%;">TasteNutri, nosso espaço de produção, curadoria e recomendação de receitas saudáveis.</p>
                </div>
                <div class="large-12 columns">
                    <ul class="loop-two owl-carousel owl-theme">
                        
                        <?php 
                        $contador = 1;
                            foreach ($json_data as $key => $value) {
                                if($contador<=3){
                                    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $value['video'], $matches);
                        ?>
                        <li>
                            <div class="box">
                                <div class="garden-img-full-width">
                                    <div style="width: 100%;height: 200px;position: absolute;"></div>
                                    <iframe style="width:100%; height: 200px;" src="https://www.youtube.com/embed/<?php echo $matches[0]; ?>" frameborder="0" allowfullscreen></iframe>
                                    <span style="display:inline"><a class="video" data-video="https://www.youtube.com/embed/<?php echo $matches[0]; ?>"  data-toggle="modal" data-target="#videoModal" ><span class="fa fa-play-circle"></span></a></span>
                                </div>
                                <h3><?php echo $value['titulo']; ?></h3>
                            </div>
                        </li>

                        <?php  
                                $contador++;
                                }     
                            }
                        ?>
                        
                    </ul>
                </div>
            </div>
        </section>

        <section id="blog" class="latest-news-v2">
            <div class="container">
                <div class="garden-title text-center">
                    <h3>Blog</h3>
                </div>

                <div class="row">

                    <div class="grid">
                        <div id="grid-interno"></div>
                    </div>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 position-relative none-mg div-form-elemento" style="margin-top: 15px; text-align: center">
                        <div class="garden-button">
                            <a id="mais-receitas" class="smoothScroll" style="background: #e6a953; color: white;">Veja Mais</a>
                        </div>
                        <div class="garden-button">
                            <a id="menos-receitas" class="smoothScroll" style="background: #2e503d; color: white; display:none">Ocultar</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="idcontato" class="contact-form">
            <div class="container col-md-12">
                <div class="box garden-form">
                    <h3 style="margin-top: 45px;">Entre em contato ou agende sua consulta</h3>
                    <form method="post" action="javascript:func()" id="contato_form" style="margin-top:10px !important" >
                        <div class="row">
                            <div class="col-md-4  position-relative div-form-elemento">
                                <input type="text"  placeholder="Nome *" name="nome" style="width:100%" required="required" class="input-form-elemento">
                            </div>
                            <div class="col-md-4  position-relative div-form-elemento">
                                <input type="text" placeholder="Telefone *" name="telefone" style="width:100%" class="input-form-elemento">
                            </div>
                            <div class="col-md-4  position-relative div-form-elemento">
                                <input type="email" placeholder="Email *" name="email" style="width:100%" required="required" class="input-form-elemento">
                            </div>
                        </div >
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 position-relative div-form-elemento">
                                <label class="webfont_calendar" style="width: 100%">
                                    <input type="text" name="data" class="input-form-elemento data-form" placeholder="Dia: dd-mm-aaaa" style="width:100%" id="datepicker">
                                </label>
                            </div>
                            <style>
                            #select-hora select:focus{
                                                    
                                outline: none;
                                border: 1px solid #ababab; 
                            }
                            </style>        
                            <div class="col-md-4 col-sm-12 col-xs-12 position-relative div-form-elemento " >
                            <div id="select-hora">
                                <input type="text" placeholder="Hora: 00:00" name="hora" style="width:100%"  class="input-form-elemento" id="horas" disabled>  
                            </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 position-relative" style="margin-top: 10px;">
                                <div class='alert alert-warning' role='alert' style='margin-bottom: -7px !important; width: 91%; position: absolute; display:none'>Escolha um dia da semana.</div>
                                <div class='alert alert-danger' role='alert' style='margin-bottom: -7px !important; width: 91%; position: absolute; display:none'>Selecione primeiro uma data.</div>
                                <div id="content-message-contact"><span style="color:red ">*</span><span style="color: white">Disponibilidade de Horário pode estar sujeita a confirmação.</span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 position-relative" style="margin-bottom: 18px;">
                                    <textarea class="col-md-12 col-sm-12 col-xs-12" name="mensagem" placeholder="Mensagem *" rows="7" required="required" style="height: 100px;"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 position-relative none-mg div-form-elemento" style="margin-top: 0px">  
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 position-relative none-mg div-form-elemento" style="margin-top: 0px">
                              <div class="garden-button">
                                 <input type="submit" id="btn-contato" value="Enviar">
                              </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 position-relative none-mg div-form-elemento" style="margin-top: -10px">
                            <div class="alert alert-success" role="alert" style="display: none; margin-bottom: -7px !important ">Mensagem enviada com sucesso!</div>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <section id="mapa" class="gmap-contact">
            <div class="gmap-wrapper">
                <div class="google-map" id="gmap_contact"></div>
            </div>
        </section>


        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content-video" style="background-color: transparent;">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; opacity: 1;"><span aria-hidden="true">X</span></button>
                <iframe width="100%" height="450" src="" frameborder="0" allowfullscreen></iframe>
            </div>
            </div>
        </div>
        </div>
        
         <!-- Modal -->
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header" style="background-color: #e6a953; font-weight: bold; font-size: 20px; color: white;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="top: 6px; position: relative;" >X</span></button>
                <h4 class="modal-title modal-titulo" id="myModalLabel" style="width: 80%" >

                </h4>
            </div>
                <div class="row">
                    <div class=" col-md-2" ></div>
                        <div class="modal-image modal-imagem col-md-8" style=" padding-left: 4%; padding-top: 10px; padding-bottom:10px; text-align:center">

                        </div>
                    <div class=" col-md-2" ></div>
                </div>
                <div class="row">
                    <div class=" col-md-1" ></div>
                        <div class="modal-body modal-corpo col-md-10" style="text-align: justify; padding-left:25px; padding-right:60px">
                    </div>
                    <div class=" col-md-1" ></div>
                </div>
           </div>
         </div>
       </div>

        

<?php get_footer(); ?>