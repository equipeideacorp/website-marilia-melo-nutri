<?php 

require 'mailer/excessoes.php';
require 'mailer/mailer.php';
require 'mailer/smtp.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["nome"])) 
{

    $nome = $_POST['nome'];
    $telefone = $_POST['telefone'];
    $email = $_POST['email'];
    $data = $_POST['data'];
    $hora = $_POST['hora'];
    $mensagem = $_POST['mensagem'];

    $para = 'falecom@mariliamelonutri.com.br';
    $assunto = 'Contato do site';
    $mensagemcompleta = 'Olá, você acabou de receber uma mensagem <br/> Nome ' . $nome . ' <br/> Telefone ' . $telefone . ' <br/> Telefone ' . $email . ' <br/> Data ' . $data . ' <br/> Hora ' . $hora . ' <br/> Mensagem ' . $mensagem;    
    
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    $sent = mail($para, $assunto, $mensagemcompleta, $headers);
    if($sent) 
    {
      echo 'Sua mensagem foi enviada!';
    } 
    else 
    {
      echo 'Desculpe, sua mensagem não foi enviada!';
    }
}

?>