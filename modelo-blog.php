<?php get_header(); 

/* Template Name: Página Home */

?>       
       
    <section id="sliderprincipal" class="rev_slider_wrapper slider-v2">
        <div id="slider-v2" class="rev_slider"  data-version="5.0">
            <ul>
                <li data-transition="random" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" >
                
                    <video loop autoplay style="width: 100%; filter: blur(10px); ">
                        <source src="<?php echo get_template_directory_uri() ?>/videos/slider.mp4" type="video/mp4">
                    </video>

                    <div id="box-slide" class="flexslider">
                        <ul class="slides">
                            <li>
                                <h3>Confira nossas</h3>
                                <h1 style="position: relative;left: -15px;">Indicações</h1>
                                <p> do TasteNutri, em nosso Canal! </br> Se inscreva e aproveite!</p>
                                    </br>
                            <div style="float:right">
                               <div class="g-ytsubscribe" data-channelid="UCUcQ7pd15nQAVyfu1piMBsQ" data-layout="default" data-count="default" ></div>
                            </div>
                            </li>
                            <li>
                                <h3>Marque já sua</h3>
                                <h1 style="position: relative;left: -15px;">Consulta!</h1>
                                <p> agende agora mesmo! </br> </p>
                                    </br>
                                <div class="garden-button" style="float:right">
                                    <a href="#idcontato" class="smoothScroll" style="background: #e6a953; color: white;">Agendar Consulta</a>
                                </div>
                            </li>
                            <li>
                                <h3>As receitas</h3>
                                <h1 style="position: relative;left: -15px;">Preparadas</h1>
                                <p>especialmente pela sua Nutri Marilia Melo.</br> </p>
                                </br>
                                <div class="garden-button" style="float:right">
                                    <a href="#tastenutri" class="smoothScroll" style="background: #c2de6f; color: white;">Clique e confira!</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </section>

   <section id="image-mobile" style="height: 600px; ">
            <div style="100%">
                <img style="height: 100%" src="<?php echo get_template_directory_uri() ?>/images/fotosaladamobile.PNG">
            </div>

            <div id="box-slide" class="flexslider-mobile">
                <ul class="slides">
                    <li>
                        <h3>Confira nossas</h3>
                        <h1 style="position: relative;left: -15px;">Indicações</h1>
                        <p> do TasteNutri, restritivas em nosso Canal! </br> Se inscreva e aproveite!</p>
                            </br>
                        <div style="float:right">
                            <div class="g-ytsubscribe" data-channelid="UCUcQ7pd15nQAVyfu1piMBsQ" data-layout="default" data-count="default" ></div>
                            </div>
                    </li>
                    <li>
                        <h3>Marque já sua</h3>
                        <h1 style="position: relative;left: -15px;">Consulta!</h1>
                        <p> agende agora mesmo! </br> </p>
                            </br>
                        <div class="garden-button" style="float:right">
                            <a href="#idcontato" class="smoothScroll" style="background: #e6a953; color: white;">Agendar Consulta</a>
                        </div>
                    </li>
                    <li>
                        <h3>As melhores</h3>
                        <h1 style="position: relative;left: -15px;">Receitas</h1>
                        <p> você encontra no site da nutri! </br> </p>
                        </br>
                        <div class="garden-button" style="float:right">
                            <a href="#tastenutri" class="smoothScroll" style="background: #c2de6f; color: white;">Clique e confira!</a>
                        </div>
                    </li>
                </ul>
            </div>
    </section>

        <section style="height: 971px; background: url(<?php echo get_template_directory_uri() ?>/images/index-2/bg-why-choose-us.jpg) no-repeat top center;" id="sobrenos" class="about-v2">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-6 col-sm-12">
                        <div class="col-md-offset-3 col-md-7 col-sm-12">
                            <img src="<?php echo get_template_directory_uri() ?>/images/fotomariliamelo.jpg">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12" style="padding-right: 10%">
                        <div style="padding-top: 10px;" class="box-content">
                            <h2 style="font-size:40px; padding-bottom: 20px; color:#FFF">Marília Melo</h2>
                            <p style="color:#FFF">Marília Melo é nutricionista formada pela Universidade de Fortaleza (UNIFOR), pós-graduada nas 
                                áreas de nutrição funcional e em nutrição esportiva. Atua em consultório com foco em emagrecimento, nutrição estética, 
                                melhora da performance esportiva, nutrição funcional, reeducação alimentar, prevenção e tratamento de doenças crônicas 
                                não transmissíveis (DCNT). </p><p style="color:#FFF"> Acredita no poder dos alimentos e que uma alimentação saudável deve ser baseada principalmente 
                                no equilíbrio e no desenvolvimento de um novo estilo de vida, respeitando a individualidade de cada um com uma nutrição 
                                personalizada.</p>

                            <div style="display:none;" class="garden-button" style="transition: none; line-height: 26px; border-width: 1px; margin: 0px; padding: 5px; letter-spacing: 0px; font-weight: 400; font-size: 14px;">
                                <a href="sobre.html" style="background-color: #2E503D; color:#FFF; transition: none; line-height: 50px; border-width: 0px; margin: 0px; padding: 0px 35px; letter-spacing: 0px; font-size: 16px;">SAIBA MAIS</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>
    <pre>
        <?php 
            $json = file_get_contents(get_site_url()."/tastenutri/json-receitas/");
            $json_data = json_decode($json, true);

            $json_todas = file_get_contents(get_site_url()."/tastenutri/json-todas-as-receitas/");
            $json_todas_data = json_decode($json_todas, true);

        ?>
</pre>

        <section id="tastenutri" class="recent-projects-v2">
            <div class="container">
                <div class="garden-title text-center">
                    <h3>Receitas da Nutri</h3>
                    <p style="text-align: center">Receitas preparadas pela sua nutri especialmente para você!</p>
                </div>
                
                <div class="garden-slick-menu">
                    <ul id="our_projects">
                        <li id="all_projects" class="active button-filter"><a href="">Todas</a></li>
                        <?php  
                        
                        foreach($json_data as $key => $value)
                        {
                            
                        ?>
                        
                            <li id="<?php echo $key; ?>" class="button-filter"><a href=""><?php echo ucfirst($key); ?></a></li>

                        <?php
                            }
                        
                        ?>
                    </ul>
                </div>
                <div class="slick-content">
                    <ul class="slick-slider slick-our-projects-v2">
                        
                        <?php
                            //<img src="'.esc_url($item["imagem"][0]).'" alt="">
                          
                            foreach ($json_todas_data as $item) 
                            {
                                
                        
                        ?> 

                                        <li class="filter-all_projects <?php echo $item['categorias']; ?>">
                                            <div class="box garden-overlay-scale">
                                                <div class="garden-img-full-width" style="width: 255px;height: 240px; background: url(<?php echo esc_url($item["imagem"][0])?>); background-size: cover;"  >
                                                    
                                                </div>
                                                <div class="garden-overlay">
                                                    <a href="" data-toggle="modal" data-target="#myModal" titulo="<?php echo $item["titulo"] ?>"  conteudo="<?php echo $item["conteúdo"] ?>"  imagem="<?php echo esc_url($item["imagem"][0]) ?>" class="modal-receita"><h3><?php echo $item["titulo"]?></h3></a>
                                                </div>
                                            </div>
                                        </li>
                        <?php
                                    

                            }

                        ?>
                    </ul>
                </div>
            </div>
        </section>
        
        <section id="servicos" class="why-choose-us">
            <div class="container">
                <div class="garden-title text-center">
                    <h3>Serviços da Nutri</h3>
                    <p>Confira as etapas da consulta nutricional:</p>
                </div>
                <ul class="row">
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                                <img style="filter: invert(1); width: 50px;" src="<?php echo get_template_directory_uri() ?>/images/checklist.png" alt="">
                            </div>
                            <div class="box-text">
                                <h3>Avaliação </br> Nutricional</h3>
                                <p>A Avaliação Nutricinoal envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Avaliação clínica;</li>
                                    <li>- Avaliação bioquímica;</li>
                                    <li>- Avaliação do consumo alimentar.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                               
                               <img style="filter: invert(1); width: 35px;" src="<?php echo get_template_directory_uri() ?>/images/balanca.png" alt="">
                            </div>
                            <div class="box-text">
                                <h3>Avaliação </br>  Física</h3>
                                <p>A Avaliação Física envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Avaliação antropométrica;</li>
                                    <li>- Avaliação da composição corporal;</li>
                                    <li>- Diagnóstico nutricional.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-6">
                        <div class="box clearfix">
                            <div class="box-icon">
                                <span class="flaticon flaticon-fruit"></span>
                            </div>
                            <div class="box-text">
                                <h3>Prescrição de Plano Alimentar</h3>
                                <p>A Prescrição de Plano Alimentar envolve:</p>
                                <ul style="color:#FFF">
                                    <li>- Plano alimentar individualizado;</li>
                                    <li>-  Orientação nutricional;</li>
                                    <li>- Sugestões de receitas.</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        
        <section id="missao" class="our-mission" style="padding-bottom: 24px;">
            <div class="container">
                <div class="box">
                    <div class="box-mission text-center">
                        <div class="garden-title">
                            <h3>Não há mudança no corpo sem mudança no comportamento!</h3>
                        </div>
                        <p>A mudança na relação com o alimento é o primeiro passo para alcançar seus objetivos </p>
                    </div>
                </div>
            </div>
        </section>

        <?php 
            $json = file_get_contents(get_site_url()."/tastenutri/json-videos/");
            $json_data = json_decode($json, true);
        ?>
        <section id="videos" class="our-videos-v2">
            <div class="container">
                <div class="garden-title text-center">
                    <img src="<?php echo get_template_directory_uri() ?>/images/tastenutri.png">
                    <p>As receitas mais saudáveis do tastemade selecionadas pela nutri!</p>
                </div>
                <div class="large-12 columns">
                    <ul class="loop-two owl-carousel owl-theme">
                        
                        <?php 
                        $contador = 1;
                            foreach ($json_data as $key => $value) {
                                if($contador<=3){
                        ?>
                        <li>
                            <div class="box">
                                <div class="garden-img-full-width">
                                    <div style="width: 100%;height: 200px;position: absolute;"></div>
                                    <iframe style="width:100%; height: 200px;" src="<?php echo $value['video']; ?>" frameborder="0" allowfullscreen></iframe>
                                    <span style="display:inline"><a class="video" data-video="<?php echo $value['video']; ?>"  data-toggle="modal" data-target="#videoModal" ><span class="fa fa-play-circle"></span></a></span>
                                </div>
                                <h3><?php echo $value['titulo']; ?></h3>
                            </div>
                        </li>

                        <?php  
                                $contador++;
                                }     
                            }
                        ?>
                        
                    </ul>
                </div>
            </div>
        </section>

        <section id="blog" class="latest-news-v2">
            <div class="container">
                <div class="garden-title text-center">
                    <h3>Novidades das Redes Sociais</h3>
                </div>

                <div class="row">

                    <?php

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/1861689894158788/posts/?fields=full_picture%2Cmessage%2Clink&access_token=281005105715384%7C1EqJBQns4oXYD8xZ5P5FUSKiank");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $data = curl_exec($ch);
                        
                        $array_fb = json_decode($data);

                        $obj2 = $array_fb->{'data'};
                        
                        for ($i = 0; $i < 4; $i++) 
                        {
                            $obj3 = $obj2[$i];
                            $imagem = $obj3->{'full_picture'};
                            $texto = $obj3->{'message'};
                            $link = $obj3->{'link'};
                            $titulo = explode("\n", $texto);
                            
                    ?>

                        <div class="col-md-3 boxcaixa" style="padding-top: 20px;padding-bottom: 30px;">
                            <div style="width:100%; height: 200px; background: url('<?php echo $imagem ?>'); background-size: cover;" class="box-image"></div>
                            <h3 style="font-size: 24px; padding-top:15px;"><span class="fa fa-facebook" style="color: #4267b2;"></span> - <?php echo $titulo[0] ?></h3>
                            <p style="padding-top:15px"><?php echo nl2br($texto) ?></p>
                            <a href="#blog">Expandir</a>
                        </div>

                    <?php
                            
                        } 
                                            
                        curl_close($ch);
                    ?>
                    
                </div>
            </div>
        </section>

        <section id="idcontato" class="contact-form">
            <div class="container">
                <div class="box garden-form">
                    <h3>ME ENVIE UMA MENSAGEM E TIRE SUAS DÚVIDAS</h3>
                    <p><span style="color:red ">*</span>Informe a data e hora que deseja realizar sua consulta, responderei em até 24h com a confirmação do agendamento.</p>
            
                    <?php echo do_shortcode('[contact-form-7 id="7" title="Formulário de contato 1"]');?>
                </div>
            </div>
        </section>


        <!-- Modal -->
        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content-video" style="background-color: transparent;">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; opacity: 1;"><span aria-hidden="true">X</span></button>
                <iframe width="100%" height="450" src="" frameborder="0" allowfullscreen></iframe>
            </div>
            </div>
        </div>
        </div>
        <?php

        ?>
         <!-- Modal -->
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header" style="background-color: #e6a953; font-weight: bold; font-size: 20px; color: white;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="top: 6px; position: relative;" >X</span></button>
                <h4 class="modal-title modal-titulo" id="myModalLabel" >

                </h4>
            </div>
                <div class="row">
                    <div class="modal-image modal-imagem col-md-6" style="padding-left: 4%; padding-top: 10px; padding-bottom:10px">

                    </div>
                    <div class="modal-body modal-corpo col-md-6" style="text-align: justify; padding-left:25px; padding-right:40px">
                
                    </div>
                </div>
           </div>
         </div>
       </div>

        
        

        <section id="mapa" class="gmap-contact">
            <div class="gmap-wrapper">
                <div class="google-map" id="gmap_contact"></div>
            </div>
        </section>

        

<?php get_footer(); ?>