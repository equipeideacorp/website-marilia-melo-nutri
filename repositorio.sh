#!/bin/bash

git add --all
git commit -m "$(date +'%d/%m/%Y')"
git push origin master
