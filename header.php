<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <?php wp_head(); ?>

        <meta charset="UTF-8">
        <title><?php echo get_bloginfo('name'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="<?php echo get_bloginfo('description') ?>">
        <meta content="IdeaCorp - Agência de Marketing Digital" name="author" />
        
        <link href="<?php echo get_template_directory_uri() ?>/plugins/normalize.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/animate.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/flaticon/font/flaticon.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/owl-carousel/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/slick/slick.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/settings.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/layers.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/navigation.css">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/font.css">
        
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" />

        <link rel="shortcut icon" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/flexslider.min.css" />


        <style>
          
            body{
                overflow-x:hidden !important;
            }
            .flexslider
            {
            
                width: 33% !important;
                height: auto !important;
                position: absolute !important;
                padding: 30px !important;
                margin: 0 0 0px !important;
                background: linear-gradient(to left, rgba(0, 0, 0, 0.5), transparent) !important;
                border:0px !important;
                bottom: 30px !important;


                
            }

            .flexslider .slides li h1
            {
                font-family: 'Poppins', sans-serif !important;
                color: #ffffff !important;
                font-size: 58px !important;
                font-weight: 700 !important;
                left: 0px !important;
                position: relative !important;
                transition: none !important;
                line-height: 90px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                text-align:right !important;
            }

            .flexslider .slides li p
            {
                transition: none !important;
                line-height: 26px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                font-weight: 600 !important;
                font-size: 24px !important;
                color: cornsilk !important;
                text-align: right !important;
            }
            
            .flexslider .slides li h3
            {
                color: #ffffff !important;
                transition: none !important;
                line-height: 60px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                font-weight: 400 !important;
                font-size: 38px !important;
                display: flex !important;
                justify-content: flex-end !important;
                align-items: center !important;
            }

            .flexslider .slides div
            {
                    text-align:right;
            }

             .flexslider-mobile
            {
            
                width: 90% !important;
                height: auto !important;
                position: absolute !important;
                padding: 30px !important;
                margin: 0 0 0px !important;
                background: linear-gradient(to left, rgba(0, 0, 0, 0.5), transparent) !important;
                border:0px !important;
                bottom: 30px !important;


                
            }

            .flexslider-mobile .slides li h1
            {
                font-family: 'Poppins', sans-serif !important;
                color: #ffffff !important;
                font-size: 40px !important;
                font-weight: 700 !important;
                left: 0px !important;
                position: relative !important;
                transition: none !important;
                line-height: 60px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                text-align:right !important;
            }

            .flexslider-mobile .slides li p
            {
                transition: none !important;
                line-height: 26px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                font-weight: 300 !important;
                font-size: 20px !important;
                color: #d3d3d3 !important;
                text-align: right !important;
            }
            
            .flexslider-mobile .slides li h3
            {
                color: #ffffff !important;
                transition: none !important;
                line-height: 60px !important;
                border-width: 0px !important;
                margin: 0px !important;
                padding: 0px !important;
                letter-spacing: 0px !important;
                font-weight: 400 !important;
                font-size: 30px !important;
                display: flex !important;
                justify-content: flex-end !important;
                align-items: center !important;
            }

            .flexslider-mobile .slides div
            {
                    text-align:right;
            }

            flex-direction-nav
            {
                display:none !important;

            }

            
            .webfont_calendar {
                position: relative;
                color: white;
            }
            .webfont_calendar:before {
                font-family: "FontAwesome";
                font-size: 18px;
                content: "\f073";
                position: absolute;
                right: 27px;
                top: 9px;   
            }

            #select-hora:focus{
            border-radius:45px !important;
            }

            .input-form-elemento{
            border: 1px solid #ababab;
            background: rgba(0, 0, 0, 0.25);
            padding: 10px 30px;
            font-size: 14px;
            color: #ffffff;
            font-weight: 400;
            height: 45px;
            line-height: 45px;
            border-radius: 45px;
            
            }
            .div-form-elemento{
            margin-top: 10px;
            margin-bottom: 10px !important;
            }

       

        </style>

    </head>

    <body >

        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>

        <header class="header-v2 stricky">
            <div class="container">
                <div class="header clearfix">
                    <div class="top-bar">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="logo">
                                    <a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/mariliamelonutrilogo2.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div style="display:none" class="info">
                                    <ul>
                                        <li>
                                            <div class="box">
                                                <div class="box-icon">
                                                    <span class="fa fa-facebook-official"></span>
                                                </div>
                                                <div class="content">
                                                    <h3>FACEBOOK</h3>
                                                    <p>\mariliamelonutri</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box">
                                                <div class="box-icon">
                                                    <span class="fa fa-instagram"></span>
                                                </div>
                                                <div class="content">
                                                    <h3>INSTAGRAM</h3>
                                                    <p>@mariliamelo.nutri</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-menu">
                        <ul class="menu">
                            <li class="active">
                                <a class="smoothScroll" href="#slider-v2">HOME</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="#sobrenos">SOBRE</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="#servicos">SERVIÇOS</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="#tastenutri">RECEITAS</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="#videos">TASTENUTRI</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="#blog">BLOG</a>
                            </li>
                            <li class="hvr-icon-drop">
                                 <a class="smoothScroll" href="#idcontato">CONTATO</a>
                            </li>
                            
                            <li style="display:none" class="search">
                                <button type="button" data-toggle="dropdown"><span class="fa fa-search"></span></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <form>
                                        <input type="text" name="search" placeholder="Buscar...">
                                    </form>
                                </div>
                            </li>

                            <li style="padding-left: 22px;">
                                <div class="box">
                                    <div style="color:#FFF" class="box-icon">
                                       <span class="fa fa-facebook-official"></span> <a href="https://www.facebook.com/Mar%C3%ADlia-Melo-Nutri-1861689894158788/" target="_blank" style="padding:0">  mariliamelonutri |  </a>
                                    </div>
                                </div>
                            </li>
                            
                            <li>
                                <div class="box">
                                    <div style="color:#FFF" class="box-icon">
                                       <span class="fa fa-instagram"></span> <a href="https://www.instagram.com/mariliamelo.nutri/?hl=pt-br" target="_blank" style="padding-left: 1px">  mariliamelo.nutri </a>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    
                    <div class="logo-mobile pull-left">
                        <a href="#"><img style="width: 80%;" src="<?php echo get_template_directory_uri() ?>/images/mariliamelonutrilogo2.png" alt=""></a>
                    </div>

                    <div class="bars pull-right" style="position: fixed;float: right;text-align: right;padding-left: 70%;">
                        <div class="search">
                            <button type="button" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <form>
                                    <input type="text" name="search" placeholder="Buscar...">
                                </form>
                            </div>
                        </div>
                        <div class="bar-mobile open">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="mobile-menu">
                        <nav class="nav-holder">
                            <ul>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#slider-v2">Home</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#sobrenos">Sobre</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#servicos">Serviços</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#tastenutri">Receitas</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#videos">TasteNutri</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#blog">Blog</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#idcontato">Contato</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>