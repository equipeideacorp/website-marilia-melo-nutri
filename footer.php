<footer class="footer-v1">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="logo">
                                    <img  src="http://mariliamelonutri.com.br/wp-content/themes/marilia/images/mariliamelonutrilogo2.png" alt="">
                                </div>
                                <div class="content">
                                    <div class="info">
                                        <ul>
                                            <p>Rua Coronel Linhares, Nº 1741, sala 504</p>
                                            <p>Aldeota CEP 60.170-241</p>
                                            <p>Fortaleza - Ceará</p>
                                            <p>(85) 3261 8265</p>
                                            <p>falecom<i class="fa fa-at" aria-hidden="true"></i>mariliamelonutri.com.br</p>
                                            </br>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <?php
                        $curl = curl_init();
                        
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=UCUcQ7pd15nQAVyfu1piMBsQ&maxResults=20&key=AIzaSyAwaSP7myNPFWFsA10WD-IO5VsEsfgLP7k",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache",
                            "postman-token: cf750e4a-586a-b4a4-fa58-fa5afcfc2970"
                            ),
                        ));
                        
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        
                        curl_close($curl);
                        
                        if ($err) {
                            echo "cURL Error #:" . $err;
                        } else {
                            $videos = json_decode($response);
                            $lista_videos = $videos->{'items'};
                            
                           
                            
                        }
                 
                       
                        ?>
                     
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="garden-title text-center" style="padding-bottom: 16px;">
                                    <h3 style="color: #FFF; text-align: left; font-size: 27px; ">Youtube</h3>
                                </div>
                                <h2 style="color:white">Se inscreva: </h2>
                                <div class="g-ytsubscribe" data-channelid="UCUcQ7pd15nQAVyfu1piMBsQ" data-layout="full" data-count="default"></div>
                                <h2 style="color:white; margin-top: 15px;">Um pouco do nosso canal:</h2>
                                
                                <iframe width="80%"  src='http://www.youtube.com/embed/<?php echo $lista_videos[0]->{'id'}->{'videoId'};?>'>
                                </iframe>
                                
                                
                                <div class="content">
                                    <div class="box-contact">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="garden-title text-center">
                                    <h3 style="color: #FFF; text-align: left; font-size: 27px;">Instagram</h3>
                                </div>
                                <div class="content">
                                    <div class="list-logo">
                                        <ul>
                                           
                                            <?php

                                                $curl = curl_init();

                                                curl_setopt_array($curl, array(
                                                CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token=183068640.4ba0563.e83e240f956645d1b61dcd38c1d4722e",
                                                CURLOPT_RETURNTRANSFER => true,
                                                CURLOPT_ENCODING => "",
                                                CURLOPT_MAXREDIRS => 10,
                                                CURLOPT_TIMEOUT => 30,
                                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                                CURLOPT_CUSTOMREQUEST => "GET",
                                                CURLOPT_HTTPHEADER => array(
                                                    "cache-control: no-cache",
                                                    "postman-token: 5276c78a-88bf-f7e0-be4d-d7131380d2de"
                                                ),
                                                ));

                                                $response = curl_exec($curl);
                                                $err = curl_error($curl);

                                                curl_close($curl);

                                                if ($err) 
                                                {
                                                    echo "cURL Error #:" . $err;
                                                } 
                                                else 
                                                {
                                                    $obj = json_decode($response);
                                                    $obj2 = $obj->{'data'};
                                                    //print_r($obj2);
                                                    for ($i = 0; $i < 6; $i++) 
                                                    {

                                                        $obj3 = $obj2[$i];
                                                        $link = $obj3->{'link'};
                                                        $obj4 = $obj3->{'images'};
                                                        $obj5 = $obj4->{'standard_resolution'}; 
                                                        $url = $obj5->{'url'};
                                            ?>
                                                    <li><a href="<?php echo $link ?>" target="about_blank"><div style="background:url(<?php echo $url ?>); background-size:cover; width:100px; height:100px"></div></a></li>

                                            <?php 
                                            

                                                    }
                                                }

                                            ?>
                                          
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright text-center">
                <p>Marília Melo Nutricionista</p>
                <ul>
                    <li><a href="https://www.facebook.com/Mar%C3%ADlia-Melo-Nutri-1861689894158788/" target="_blank"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="https://www.instagram.com/mariliamelo.nutri/?hl=pt-br" target="_blank"><span class="fa fa-instagram"></span></a></li>
                </ul>
            </div>
</footer>

        <?php wp_footer(); ?>

        <div class="backtotop"><span class="fa fa-long-arrow-up"></span></div>
        <script src="https://apis.google.com/js/platform.js"></script>
        
        <script src='http://code.jquery.com/jquery-latest.js'/>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.5.0/velocity.min.js'/>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/owl-carousel/dist/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/slick/slick.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/jquery-waypoints/2.0.3/waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/jquery-count-to-number/jquery.counterup.min.js" type="text/javascript"></script>

        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/jquery-ui.js"></script>
                                               
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=devanagari,latin-ext" rel="stylesheet">

        <script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_bXG-q5B71txCRE36VnP07Q3r8H_VPVE&libraries=geometry&callback=initMap"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.0/masonry.pkgd.min.js"></script>

        <script>
            $(document).ready(function(){
                $("div.tp-mask-wrap").css("overflow", 'visible');
            });
        </script>

        <script>
            window.fbAsyncInit = function() {
                FB.init({
                appId      : '281005105715384',
                xfbml      : true,
                version    : 'v2.10'
                });
                FB.AppEvents.logPageView();
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        

        <style>

            body > footer > div.footer > div > div > div:nth-child(3) > div > div.content > div > ul > li{margin: 0 12px 9px 0 !important;}

            #box-slide > ol > li > a.flex-active {
                background-color: white !important;
            }


        </style>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109139148-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-109139148-1');
        </script>

    </body>
</html>
