<?php

    require_once("/srv/users/mariliamelonutri/public/wp-load.php");

    $quantidade = $_POST['quantidade'];
    
    $args = array(
        'posts_per_page' => -1,
        'post_type'         => array('facebook', 'instagram'),  
    );

    $the_query = new WP_Query( $args );

    $arrayFacebook = array();

    $array_fb = array();
    $array_in = array();

    if ( $the_query->have_posts() ) 
    {
        while ( $the_query->have_posts() ) 
        {
            $the_query->the_post();
            
            $titulo = get_the_title();
            $imagem = get_field('imagem');
            $texto = apply_filters('the_content', get_post_field('post_content',get_the_id(),'db'));
            $tipo = get_field('redesocial');

            if ($tipo=="facebook")
            {
               $array_temp = array();
               array_push($array_temp, $titulo);
               array_push($array_temp, $imagem);
               array_push($array_temp, $texto);
               array_push($array_temp, $tipo);
               array_push($array_fb, $array_temp);
            }
            else
            {
                $array_temp2 = array();
                array_push($array_temp2, $titulo);
                array_push($array_temp2, $imagem);
                array_push($array_temp2, $texto);
                array_push($array_temp2, $tipo);
                array_push($array_in, $array_temp2);
            }
            
        }
        
        wp_reset_postdata();
    }


    for ($i=0; $i < $quantidade ; $i++) { 
        
        if($array_fb[$i]){
            
            $retorno .= '<div class="grid-item caixa-branca">
                <div class="efeito" style="height: 215px">
                    <div class="img-full efeito2" style="background:url('.$array_fb[$i][1].'); background-size: cover; height:285px !important"></div>
                </div>
                <div class="caixa"><i class="fa fa-facebook" aria-hidden="true"></i>
                '.$array_fb[$i][2].'
                </div>
            </div>';
        }  

        if($array_in[$i]){
            
            $retorno .= '<div class="grid-item caixa-branca">
                <div class="efeito" style="height: 215px">
                    <div class="img-full efeito2" style="background:url('.$array_in[$i][1].'); background-size: cover; height:285px !important"></div>
                </div>
                <div class="caixa"><i class="fa fa-instagram" aria-hidden="true"></i>
                '.$array_in[$i][2].'
                </div>
            </div>';
        }

    }

    echo $retorno;
    
?>