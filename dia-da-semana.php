<?php 

require_once("/srv/users/mariliamelonutri/public/wp-load.php");

$variable = $_POST['data'];

$diaDaSemana = date('w', strtotime($variable));

if($diaDaSemana != 6 && $diaDaSemana != 0){
    $args = array(

        'post_type' => 'datas',
        'meta_key'  => 'dia',
        'meta_value'=> $diaDaSemana

    );

    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) 
    {
        
        $texto = '<select id="horas" class="input-form-elemento" name="hora" style="width:100% ">';
        
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            
            if (get_field('08')==True)
            {
                $texto .= '<option value="08">08:00</option>';
            }

            if (get_field('09')==True)
            {
                $texto .= '<option value="09">09:00</option>';
            }

            if (get_field('10')==True)
            {
                $texto .= '<option value="10">10:00</option>';
            }

            if (get_field('11')==True)
            {
                $texto .= '<option value="11">11:00</option>';
            }

            if (get_field('14')==True)
            {
                $texto .= '<option value="14">14:00</option>';
            }

            if (get_field('15')==True)
            {
                $texto .= '<option value="15">15:00</option>';
            }

            if (get_field('16')==True)
            {
                $texto .= '<option value="16">16:00</option>';
            }
            
        }
        
        $texto .= '</select>';

        wp_reset_postdata();
    }
} else{
    $texto = 8;
}

echo $texto;

?>