<?php
    
    function func_datas() {
        
            $labels = array(
                'name'                  => _x( 'Datas', 'Post Type General Name', 'datas' ),
                'singular_name'         => _x( 'Datas', 'Post Type Singular Name', 'datas' ),
                'menu_name'             => __( 'Datas', 'datas' ),
                'name_admin_bar'        => __( 'Data', 'datas' ),
                'archives'              => __( 'Arquivos', 'datas' ),
                'attributes'            => __( 'Atributos', 'datas' ),
                'parent_item_colon'     => __( 'Parentes', 'datas' ),
                'all_items'             => __( 'Todos', 'datas' ),
                'add_new_item'          => __( 'Adicionar', 'datas' ),
                'add_new'               => __( 'Adicionar', 'datas' ),
                'new_item'              => __( 'Adicionar', 'datas' ),
                'edit_item'             => __( 'Editar', 'datas' ),
                'update_item'           => __( 'Atualizar', 'datas' ),
                'view_item'             => __( 'Visualizar', 'datas' ),
                'view_items'            => __( 'Visualizar', 'datas' ),
                'search_items'          => __( 'Procurar', 'datas' ),
                'not_found'             => __( 'Nada encontrado.', 'datas' ),
                'not_found_in_trash'    => __( 'Nada encontrado', 'datas' ),
                'featured_image'        => __( 'Imagem', 'datas' ),
                'set_featured_image'    => __( 'Definir Imagem', 'datas' ),
                'remove_featured_image' => __( 'Remover Imagem', 'datas' ),
                'use_featured_image'    => __( 'Use o recurso de imagem', 'datas' ),
                'insert_into_item'      => __( 'Inserir item', 'datas' ),
                'uploaded_to_this_item' => __( 'Atualizar esse item', 'datas' ),
                'items_list'            => __( 'Listar Itens', 'datas' ),
                'items_list_navigation' => __( 'Navegar nos itens', 'datas' ),
                'filter_items_list'     => __( 'Filtrar os itens', 'datas' ),
            );
            $rewrite = array(
                'slug'                  => 'datas',
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
            );
            $args = array(
                'label'                 => __( 'datas', 'datas' ),
                'description'           => __( 'datas', 'datas' ),
                'labels'                => $labels,
                'supports'              => array( ),
                'taxonomies'            => array( 'category' ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'dashicons-products',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,		
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'page',
                'show_in_rest'          => true,
                'rest_base'             => 'datas-resti',
            );
            register_post_type( 'datas', $args );
        
        }
        add_action( 'init', 'func_datas', 0 );

?>