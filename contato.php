<?php /* Template Name: Contato */ ?>

<!DOCTYPE html>
<html lang="pt-br">
    
    <head>
        <meta charset="UTF-8">
        <title>Marilia Melo Nutri</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        
        <link href="<?php echo get_template_directory_uri() ?>/plugins/normalize.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/animate.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/flaticon/font/flaticon.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/owl-carousel/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri() ?>/plugins/slick/slick.css" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/settings.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/layers.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/plugins/revolution/css/navigation.css">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/font.css">
        
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" />


    </head>

    <body>

        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>

        <header class="header-v2 stricky">
            <div class="container">
                <div class="header clearfix">
                    <div class="top-bar">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="logo">
                                    <a href="index.html"><img src="<?php echo get_template_directory_uri() ?>/images/mariliamelonutrilogo2.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div style="display:none" class="info">
                                    <ul>
                                        <li>
                                            <div class="box">
                                                <div class="box-icon">
                                                    <span class="fa fa-facebook-official"></span>
                                                </div>
                                                <div class="content">
                                                    <h3>FACEBOOK</h3>
                                                    <p>\mariliamelonutri</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box">
                                                <div class="box-icon">
                                                    <span class="fa fa-instagram"></span>
                                                </div>
                                                <div class="content">
                                                    <h3>INSTAGRAM</h3>
                                                    <p>@mariliamelonutri</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-menu">
                        <ul class="menu">
                            <li class="active">
                                <a class="smoothScroll" href="index.html">HOME</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="index.html#sobrenos">SOBRE</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="index.html#tastenutri">TASTENUTRI</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="index.html#servicos">SERVIÇOS</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="index.html#videos">VÍDEOS</a>
                            </li>
                            <li class="hvr-icon-drop">
                                <a class="smoothScroll" href="index.html#blog">BLOG</a>
                            </li>
                            <li class="hvr-icon-drop">
                                 <a class="smoothScroll" href="contato.html">CONTATO</a>
                            </li>
                            
                            <li style="display:none" class="search">
                                <button type="button" data-toggle="dropdown"><span class="fa fa-search"></span></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <form>
                                        <input type="text" name="search" placeholder="Buscar...">
                                    </form>
                                </div>
                            </li>

                            <li style="padding-left: 50px;">
                                <div class="box">
                                    <div style="color:#FFF" class="box-icon">
                                        <span class="fa fa-facebook-official"></span> mariliamelonutri |  
                                    </div>
                                </div>
                            </li>
                            
                            <li>
                                <div class="box">
                                    <div style="color:#FFF" class="box-icon">
                                        <span class="fa fa-instagram"></span> mariliamelonutri
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    
                    <div class="logo-mobile pull-left">
                        <a href="#"><img style="width: 80%;" src="images/mariliamelonutrilogo2.png" alt=""></a>
                    </div>

                    <div class="bars pull-right" style="position: fixed;float: right;text-align: right;padding-left: 70%;">
                        <div class="search">
                            <button type="button" data-toggle="dropdown"></button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <form>
                                    <input type="text" name="search" placeholder="Buscar...">
                                </form>
                            </div>
                        </div>
                        <div class="bar-mobile open">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="mobile-menu">
                        <nav class="nav-holder">
                            <ul>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="index.html">Home</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#sobrenos">Sobre</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#tastenutri">Receitas</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#servicos">Serviços</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#videos">TasteNutri</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="#blog">Blog</a>
                                </li>
                                <li class="has-submenu garden-hv-child-color-primary">
                                    <a class="smoothScroll" href="contato.html">Contato</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="rev_slider_wrapper slider-v2">
            <div id="slider-v2" class="rev_slider"  data-version="5.0">
                <ul>
                    <li data-transition="random" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" >
                        <img src="images/sobre.jpg">
                    </li>
                   
                </ul>
            </div>
        </section>

        <section id="info" class="contact-info garden-set-pd-sm">
            <div class="container">
                <div class="title">
                    <h3>ENTRE EM CONTATO</h3>
                    <p>Utilize o formulário abaixo para entrar em contato!</p>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="box">
                            <div class="box-icon">
                                <span class="fa fa-map-marker"></span>
                            </div>
                            <div class="box-content">
                                <h3>Endereço</h3>
                                <p>Rua Coronel Linhares N1741 - Sala 504 - Aldeota</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="box">
                            <div class="box-icon">
                                <span class="fa fa-phone"></span>
                            </div>
                            <div class="box-content">
                                <h3>Telefone</h3>
                                <p>+55 85 3261.8265</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       
        <section class="contact-form garden-set-pd">
            <div class="container">
                <div class="box garden-form">
                    <h3>ENVIAR UMA MENSAGEM</h3>
                    <p><span>*</span> são necessários.</p>
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 position-relative">
                                <textarea class="col-md-12 col-sm-12 col-xs-12" name="message" rows="7" required="required"></textarea>
                                <p>Mensagem <span>*</span></p>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 position-relative">
                                <input class="col-md-12 col-sm-12 col-xs-12" type="text" name="name" required="required">
                                <p>Nome <span>*</span></p>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 position-relative">
                                <input class="col-md-12 col-sm-12 col-xs-12" type="email" name="email" required="required">
                                <p>E-mail <span>*</span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 position-relative none-mg">
                                <div class="garden-button">
                                    <a href="#">ENVIAR MENSAGEM</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <section class="working-time-v2 garden-set-pd-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="box-time">
                            <h3>HORÁRIO DE ATENDIMENTO</h3>
                            </br>
                            <ul>
                                <li><p>SEG - SEX<i>8:00 - 17:00</i></p></li>
                                <li><p>SAB<i>08:00 - 12:00</i></p></li>
                                <li><p>DOM<i>FECHADO</i></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="box-content">
                            
                            <h3 style="font-size:25px; padding-bottom:15px;">Atendimento de Horário Personalizado!</h3>
                            
                            <p style="padding-bottom:15px;">Todo e qualquer atendimento deve ser pre-agendado, isso ajuda aos nossos clientes a sempre serem atendimentos o mais rápido possível...
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pulvinar, nisl at rutrum feugiat, dolor ante convallis purus, feugiat ultrices 
                            purus felis nec orci. </p>
                            
                            <p>In laoreet elit ut elit pretium, vel congue elit pharetra.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pulvinar, nisl 
                            at rutrum feugiat, dolor ante convallis purus, feugiat ultrices purus felis nec orci. In laoreet elit ut elit pretium, vel congue elit pharetra. 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pulvinar, nisl at rutrum feugiat, dolor ante convallis purus, feugiat ultrices purus 
                            felis nec orci. In laoreet elit ut elit pretium, vel congue elit pharetra. </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="gmap-contact">
            <div class="gmap-wrapper">
                <div class="google-map" id="gmap_contact"></div>
            </div>
        </section>

        <footer class="footer-v1">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="logo">
                                    <a href="#"><img src="images/mariliamelonutrilogo2.png" alt=""></a>
                                </div>
                                <div class="content">
                                    <div class="info">
                                        <ul>
                                            <p>Encontre a localização de nosso escritório.</p>
                                            <p>Rua Coronel Linhares N1741 sala 504, Aldeota</p>
                                            </br>
                                            <iframe
                                                frameborder="0" style="border:0"
                                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyACIPCUszSg0BxRWPHUAeiUyTW1DQ0Krv8
                                                &q=Rua+Coronel+Linhares,+1741+-+504+-+Dionísio+Torres,+Fortaleza+-+CE" allowfullscreen>
                                            </iframe>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="garden-title text-center">
                                    <h3 style="color: #FFF; text-align: left; font-size: 27px;">Novidades</h3>
                                </div>
                                <div class="content">
                                    <div class="box-contact">
                                        <ul>
                                            <li><p><span class="fa fa-twitter"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
                                            <li><p><span class="fa fa-twitter"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
                                            <li><p><span class="fa fa-twitter"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
                                            <li><p><span class="fa fa-twitter"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="box">
                                <div class="garden-title text-center">
                                    <h3 style="color: #FFF; text-align: left; font-size: 27px;">Instagram</h3>
                                </div>
                                <div class="content">
                                    <div class="list-logo">
                                        <ul>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/abobrinha-ao-alho.jpg" alt=""></a></li>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/caldo-de-mandioca.jpg" alt=""></a></li>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/farofa-de-berinjela.jpg" alt=""></a></li>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/tilapia-ao-forno.jpg" alt=""></a></li>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/quijadinha-arroz-integral.jpg" alt=""></a></li>
                                            <li><a href="#"><img style="width: 100px;" src="images/receitas/risoto-de-frango.jpg" alt=""></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright text-center">
                <p>Marília Melo Nutricionista</p>
                <ul>
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                </ul>
            </div>
        </footer>

        <div class="backtotop"><span class="fa fa-long-arrow-up"></span></div>
        <script src="https://apis.google.com/js/platform.js"></script>
        <script src='http://code.jquery.com/jquery-latest.js'/>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/velocity/1.5.0/velocity.min.js'/>
        
        <script src="<?php echo get_template_directory_uri() ?>/plugins/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/owl-carousel/dist/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/slick/slick.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/jquery-waypoints/2.0.3/waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/jquery-count-to-number/jquery.counterup.min.js" type="text/javascript"></script>

        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>


    
        <script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>

        <script>
            $(document).ready(function(){
                $("div.tp-mask-wrap").css("overflow", 'visible');
            });
        </script>

    </body>
</html>
